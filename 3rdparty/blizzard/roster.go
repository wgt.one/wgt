package blizzard

import (
	"net/url"
	"strings"
)

type rosterResponse struct {
	ID      int64           `json:"id"`
	Name    string          `json:"name"`
	Members []*rosterMember `json:"members"`
}

type rosterMember struct {
	Character struct {
		Name  string `json:"name"`
		Level uint8  `json:"level"`
	}
	Rank uint8 `json:"rank"`
}

// GetRoster - get guild roster
func GetRoster(name string, realm string, region string) ([]*GuildMember, error) {
	members := []*GuildMember{}
	response := rosterResponse{}
	name = url.PathEscape(strings.ReplaceAll(strings.ToLower(name), " ", "-"))
	err := Request(region, "/data/wow/guild/"+realm+"/"+name+"/roster", "profile", &response)
	if err != nil {
		return members, err
	}
	for _, member := range response.Members {
		members = append(members, &GuildMember{
			Name:  member.Character.Name,
			Level: member.Character.Level,
			Rank:  member.Rank,
		})
	}
	return members, nil
}
