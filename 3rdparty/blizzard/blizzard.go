package blizzard

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/wgt.one/wgt/3rdparty/blizzard/oauth2"
)

var client = &http.Client{}

func getEndpoint(region string) string {
	return "https://" + region + ".api.blizzard.com"
}

// Init Blizzard API wrapper
func Init(credentials []oauth2.Credential) {
	oauth2.SetCredentials(credentials)
}

// Request - send request to blizzard API
func Request(region string, uri string, namespace string, response interface{}) error {
	endpoint := getEndpoint(region) + uri
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		return err
	}

	query := req.URL.Query()
	query.Add("locale", "en_US")
	query.Add("namespace", namespace+"-"+region)
	query.Add("access_token", oauth2.GetToken())
	req.URL.RawQuery = query.Encode()

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	switch res.StatusCode {
	case 404:
		return fmt.Errorf("Not found")
	case 429:
		time.Sleep(1 * time.Second)
		return Request(region, uri, namespace, response)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(body, response)
	if err != nil {
		return err
	}
	return nil
}
