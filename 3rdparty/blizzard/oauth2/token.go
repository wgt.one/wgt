package oauth2

import (
	"fmt"
	"math/rand"
	"time"
)

// Credential - OAuth2 credentials, client id and client secret
type Credential struct {
	ID          string
	Secret      string
	AccessToken string
}

// Pool of OAuth2 credentials
var pool = []Credential{}

// SetCredentials - add OAuth2 credentials to the pool
func SetCredentials(credentials []Credential) {
	for _, credential := range credentials {
		if isUnique(credential) {
			pool = append(pool, credential)
		}
	}
}

// RemoveToken - remove access token from pool
func RemoveToken(token string) {
	for idx, credential := range pool {
		if credential.AccessToken == token {
			pool[idx] = pool[len(pool)-1]
			pool = pool[:len(pool)-1]
			return
		}
	}
}

// RemoveCredential - remove credential without access token
func RemoveCredential(credential Credential) {
	for idx, item := range pool {
		if credential.ID == item.ID && credential.Secret == item.Secret {
			pool[idx] = pool[len(pool)-1]
			pool = pool[:len(pool)-1]
			return
		}
	}
}

// GetToken - get random access token from pool
func GetToken() string {
	if len(pool) == 0 {
		panic("[3rdparty/blizzard] no valid credentials available")
	}

	rand.Seed(time.Now().Unix())
	credential := &pool[rand.Intn(len(pool))]
	if credential.AccessToken != "" {
		return credential.AccessToken
	}

	token, err := GenerateToken(credential.ID, credential.Secret)
	if err != nil {
		fmt.Println("[3rdparty/blizzard/oauth2] GenerateToken failed:", err)
		// If credential is not valid, remove it from pool
		if token == "unauthorized" {
			RemoveCredential(*credential)
		}
		return GetToken()
	}
	credential.AccessToken = token
	return credential.AccessToken
}

// Check if credential is unique
func isUnique(credential Credential) bool {
	for _, item := range pool {
		if item.ID == credential.ID && item.Secret == credential.Secret {
			return false
		}
	}

	return true
}
