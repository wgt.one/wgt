package oauth2

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

var client = &http.Client{}

// {region}.battle.net/oauth/token response
type tokenResponse struct {
	AccessToken string `json:"access_token"`
}

type errorResponse struct {
	Error   string `json:"error"`
	Message string `json:"error_description"`
}

// GenerateToken - Get Access Token from OAuth2 credentials
func GenerateToken(clientID string, clientSecret string) (string, error) {
	header := base64.StdEncoding.EncodeToString([]byte(clientID + ":" + clientSecret))
	data := url.Values{}
	data.Set("grant_type", "client_credentials")
	req, err := http.NewRequest("POST", "https://us.battle.net/oauth/token", strings.NewReader(data.Encode()))
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Basic "+header)
	req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	if res.StatusCode == 401 {
		response := &errorResponse{}
		json.Unmarshal(body, response)
		return response.Error, fmt.Errorf(response.Error + ": " + response.Message)
	}
	response := &tokenResponse{}
	err = json.Unmarshal(body, response)
	if err != nil {
		return "", err
	}
	return response.AccessToken, nil
}
