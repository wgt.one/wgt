package blizzard

// GuildMember - guild member
type GuildMember struct {
	Name  string `json:"name"`
	Level uint8  `json:"level"`
	Rank  uint8  `json:"rank"`
}
