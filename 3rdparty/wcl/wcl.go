package wcl

import "gitlab.com/rakshazi/warcraftlogs-api"

// prefix for logs
var prefix = "[3rdparty/wcl]"

// GuildEndpoint - WarcraftLogs html guild page endpoint/prefix
var GuildEndpoint map[string]string

// V1 - WarcraftLogs APIv1 clients
var V1 map[string]*warcraftlogs.WarcraftLogs

func init() {
	GuildEndpoint = make(map[string]string)
	GuildEndpoint["retail"] = "https://www.warcraftlogs.com/guild/"
	GuildEndpoint["classic"] = "https://classic.warcraftlogs.com/guild/"

	V1 = make(map[string]*warcraftlogs.WarcraftLogs)
	V1["retail"] = warcraftlogs.NewRetail("")
	V1["classic"] = warcraftlogs.NewClassic("")
}
