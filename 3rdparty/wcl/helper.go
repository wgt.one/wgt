package wcl

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// Parsed json from guild page header
type guildPageJSON struct {
	GuildID int `json:"guildId,omitempty"`
}

// GetGuildID - Get WarcraftLogs Guild ID from html page
func GetGuildID(guild *configuration.Guild) (int, error) {
	var guildID int
	// Get Guild ID from guild page (yeah, i know, but API has no such information)
	guildPage := GuildEndpoint[guild.Game] + guild.Region + "/" + guild.Realm + "/" + url.PathEscape(guild.Name)
	req, err := http.NewRequest("GET", guildPage, nil)
	if err != nil {
		fmt.Println(prefix, err)
		return 0, err
	}

	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(prefix, err)
		return 0, err
	}
	if res.StatusCode != 200 {
		switch res.StatusCode {
		case 400:
			err = fmt.Errorf("not found on WarcraftLogs")
			fmt.Println(prefix, err)
			return 0, err
		case 429:
			fmt.Println(prefix, "too many requests")
			time.Sleep(1 * time.Minute)
			return GetGuildID(guild)
		}
	}
	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		fmt.Println(prefix, err)
		return 0, err
	}
	// Check if guild page not found.
	// Why doc.Find? Because WCL website does not provide 404 or any other way
	// to simplify handling of such cases
	notFound := false
	doc.Find("div#content p").Each(func(index int, item *goquery.Selection) {
		if notFound {
			return
		}
		notFound = strings.Contains(item.Text(), "No guild could be found with that name")
	})
	if notFound {
		err = fmt.Errorf("guild not found on warcraftlogs")
		fmt.Println(prefix, err.Error())
		return 0, err
	}

	doc.Find("script").Each(func(index int, item *goquery.Selection) {
		if !strings.Contains(item.Text(), "guildProfilePageProps") {
			return
		}
		jsonText := strings.Replace(item.Text(), "_guildProfilePageProps =", "", 1)
		jsonText = strings.Replace(jsonText, "};", "}", 1)
		guildPage := guildPageJSON{}
		json.Unmarshal([]byte(jsonText), &guildPage)
		guildID = guildPage.GuildID
	})

	return guildID, nil
}

// ValidKey - check if APIv1 key is valid
func ValidKey(key string) bool {
	if len(key) != 32 {
		return false
	}
	endpoint := "https://www.warcraftlogs.com/v1/classes?api_key=" + key
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		return false
	}

	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return false
	}
	if res.StatusCode != 200 {
		return false
	}
	return true
}
