# [WGT website](https://wgt.one) [![GoDoc](https://godoc.org/gitlab.com/wgt.one/wgt?status.svg)](https://godoc.org/gitlab.com/wgt.one/wgt)
[![Patreon](https://img.shields.io/badge/donate-patreon-orange.svg?style=for-the-badge)](https://patreon.com/rakshazi)
[![PayPal](https://img.shields.io/badge/donate-paypal-blue.svg?style=for-the-badge)](https://paypal.me/rakshazi)
[![Liberapay](https://img.shields.io/badge/donate-liberapay-yellow.svg?style=for-the-badge)](https://liberapay.com/rakshazi)



This program will generate your guild's raid attendance list with twink matches and update your guild's progress on Raider.IO, WoWProgress, WarcraftLogs

## Configuration

### json file

> **NOTE**: optional, you may use env vars (scroll down)

This tool may use a `wgt.json` file with following structure:

```json
{
    "mongo": {
        "url": "mongodb url",
        "user": "mongodb user",
        "password": "mongodb PLAIN TEXT password WITHOUT encoding",
        "database": "mongodb database name",
        "protocol": "mongodb+srv",
        "collection": "mongodb collection name",
        "collection_benches": "mongodb benches collection name"
    },
    "blizzard": {
        "client_id": "API client id",
        "client_secret": "API client secret"
    },
    "warcraftlogs": {
        "key": "API v1 key",
        "client_id": "API v2 client id",
        "client_secret": "API v2 client secret"
    },
    "export": {
        "path": "/tmp/",
        "template": {
            "attendance": "export/views/attendance.html",
            "history": "export/views/attendance.history.html"
        }
    }
}
```

* mongo - mongodb config, if you want to use mongo instead of file storage
* * url - mongo server URL, without credentials or params (check example above).
* * user - username
* * protocol - mongodb url prefix, eg: `mongodb+srv` or `mongodb`
* * password - plaintext password without encoding
* * database - database name
* * collection - collection name
* * collection_benches - beches collection name
* warcraftlogs - WarcraftLogs API integration
* * key - [Wacraft Logs API v1 Key](https://www.warcraftlogs.com/profile#api-title)
* * client_id - WarcraftLogs API v2 Client ID
* * client_secret - WarcraftLogs API v2 Client Secret
* blizzard - blizzard api integration
* * client_id - Blizzard OAuth2 Client ID
* * client_secret - Blizzard Oauth2 Client Secret
* export - export settings
* * path - path to export dir
* * template - templates config
* * * attendance - attendance (main table) html template
* * * history - attendance history (per-raid) html template

You can also specify the config file path with the `-config` flag. For example:

```bash
wgt -config my/custom/dir/my_config.json
```

Check more configuration options in the [wgt.json](wgt.json)

### environment variables

List of available variables:

```bash
# WarcraftLogs
WGT_WARCRAFTLOGS_KEY=
WGT_WARCRAFTLOGS_CLIENT_ID=
WGT_WARCRAFTLOGS_CLIENT_SECRET=

# Sentry
WGT_SENTRY_DSN=

# MongoDB
WGT_MONGO_URL=
WGT_MONGO_USER=
WGT_MONGO_PROTOCOL=
WGT_MONGO_PASSWORD=
WGT_MONGO_DB=
WGT_MONGO_COLLECTION=
WGT_MONGO_COLLECTION_BENCHES=

# Blizzard
WGT_BLIZZARD_CLIENT_ID=
WGT_BLIZZARD_CLIENT_SECRET=

# WGT export
WGT_EXPORT_PATH=
WGT_EXPORT_TEMPLATE_ATTENDANCE=
WGT_EXPORT_TEMPLATE_HISTORY=
```

### Guild config (mongo)

* name - guild name
* game - WoW release, `retail` or `classic` (default is retail)
* realm - guild realm, Waracraft Logs slug (eg: russian realms must replace "й" to "и")
* region - guild region
* vanity - custom filename/vanity url for guild results
* integration - _OPTIONAL_ special values for 3rdParty services. _NOTE_: if mongodb is used, integration values will be set automatically (and saved to mongo)
* * apikey - _OPTIONAL_ Guild's own [Wacraft Logs API Key](https://www.warcraftlogs.com/profile#api-title)
* * blizzard - _OPTIONAL_ Guild's own Blizzard OAuth2 credentials
* * realmId - _OPTIONAL_ Raider.io realm id
* * guildId - _OPTIONAL_ WarcraftLogs.com guild id
* attendance - attendance tracker config
* * maxReports - how many warcraftlogs reports should be included
* * raids - list of raids to work with. 24 - Nyalotha, 23 - The Eternal Palace, 22 - Crucible of Storms, 21 - BoD, 19 - Uldir, 1000x - classic (check warcraftlogs api for exact ids)
* * bench - Autobench character, if he did not participate in raid night, useful if you have several guys who come to each RT, but "sit on bench" (playing in other games, as example) and wait for Raid Leader to invite them
* * ignore - filters
* * days - ignore reports recorded in following days. 0 - Sunday, ... [go time.Weekday](https://golang.org/pkg/time/#Weekday)
* * date - ignore any reports before that date, format: yyyy-mm-dd
* * threshold - ignore characters with attendance percent lower than that value
* * modes - ignore fights with following difficulties. 1 - LFR, 2 - Flex, 3 - Normal, 4 - Heroic, 5 - Mythic, 10 - Challenge/Keystone/Mythic+
* * nonguild - ignore non-guild characters, default: false
* alts - list of match pairs. Example: you have main char `Etke` and alts `Sivu` and `Zirel`, but you go to raid with all of these chars,
so RL must respect your alts attendance and merge it with your main's attendance manually. Now, you just set list of your twinks and RL will be happy

### Commands

#### progress

will update guilds' progress, example:

```bash
wgt progress
```

### attendance

will analyze guilds' attendance, example:

```bash
wgt attendance
```

## Run

### Golang

```bash
go get gitlab.com/wgt.one/wgt
cd $GOPATH/src/gitlab.com/wgt.one/wgt
go install
curl https://gitlab.com/wgt.one/wgt/raw/master/wgt.json -o wgt.json
wgt
```

### Binary

```bash
curl https://bin.wgt.one/app --output wgt
curl https://gitlab.com/wgt.one/wgt/raw/master/wgt.json -o wgt.json
chmod +x ./wgt
./wgt
```

### Docker

```bash
curl https://gitlab.com/wgt.one/wgt/raw/master/wgt.json -o wgt.json
docker run -d -v "$PWD/wgt.json:/opt/app/wgt.json" registry.gitlab.com/wgt.one/wgt
```

