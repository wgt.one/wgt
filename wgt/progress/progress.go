package progress

import (
	"fmt"
	"sync"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"gitlab.com/wgt.one/wgt/wgt/workerpool"
)

// RunPool - run progress updater workers pool
func RunPool(workers int, guilds []*configuration.Guild) {
	fmt.Println("[wgt/progress] starting pool")
	pool := workerpool.New()
	pool.Run(workers, guilds, Run)
}

//Run updater with provied configuration
func Run(parentWG *sync.WaitGroup, queue chan *configuration.Guild, done chan *configuration.Guild) {
	defer parentWG.Done()
	for guild := range queue {
		var wg sync.WaitGroup
		wg.Add(1)
		go warcraftlogs(guild, &wg)

		if guild.Game == "retail" {
			wg.Add(1)
			go wowprogress(guild, &wg)
			wg.Add(1)
			go raiderio(guild, &wg)
		}

		wg.Wait()
		fmt.Println("[wgt/progress]", guild.Name, "done")
	}
}
