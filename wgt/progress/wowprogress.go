package progress

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

//Update Wowprogress.com guild progress
func wowprogress(guild *configuration.Guild, wg *sync.WaitGroup) {
	urlString := "https://www.wowprogress.com/update_progress/guild/" + guild.Region + "/" + url.PathEscape(guild.Realm) + "/" + url.PathEscape(guild.Name)
	data, err := wowprogressGetRequest(urlString)
	if err != nil {
		fmt.Println("[wgt/progress] wowprogress failed for", guild.Name, err)
		return
	}

	req, err := http.NewRequest("POST", urlString, strings.NewReader(data.Encode()))
	if err != nil {
		fmt.Println("[wgt/progress] wowprogress failed for", guild.Name, err)
		wg.Done()
		return
	}
	req.Header.Set("X-Requested-With", "XMLHttpRequest")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Set("Referer", urlString)

	client := &http.Client{}
	_, err = client.Do(req)
	if err != nil {
		fmt.Println("[wgt/progress] wowprogress failed for", guild.Name, err)
	}
	wg.Done()
}

// Prepare Wowprogress Request body - grab characters ids
// from update page and return as url.Values object to use
// in http.Request
func wowprogressGetRequest(urlString string) (url.Values, error) {
	data := url.Values{}
	data.Set("submit", "1")
	doc, err := goquery.NewDocument(urlString)
	if err != nil {
		return data, err
	}
	doc.Find("input.char_chbx").Each(func(index int, item *goquery.Selection) {
		id, _ := item.Attr("id")
		id = strings.ReplaceAll(id, "check_", "")
		data.Add("char_ids", string(id))
	})
	return data, nil
}
