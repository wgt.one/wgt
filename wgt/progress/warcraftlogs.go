package progress

import (
	"fmt"
	"net/http"
	"strconv"
	"sync"

	"gitlab.com/wgt.one/wgt/3rdparty/wcl"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// Update guild progress on warcraftlogs
func warcraftlogs(guild *configuration.Guild, wg *sync.WaitGroup) {
	url := wcl.GuildEndpoint[guild.Game] + "update/" + strconv.Itoa(guild.Integration.GuildID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("[wgt/progress] warcraftlogs failed for", guild.Name, err)
		wg.Done()
		return
	}

	client := &http.Client{}
	_, err = client.Do(req)
	if err != nil {
		fmt.Println("[wgt/progress] warcraftlogs failed for", guild.Name, err)
	}
	wg.Done()
}
