package progress

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

//Update Raider.io guild progress
func raiderio(guild *configuration.Guild, wg *sync.WaitGroup) {
	url := "https://raider.io/api/crawler/guilds"
	body := struct {
		Guild   string `json:"guild"`
		Members int    `json:"numMembers"`
		Realm   string `json:"realm"`
		Region  string `json:"region"`
		RealmID int    `json:"realmId"`
	}{
		Guild:   guild.Name,
		Members: 0,
		Realm:   guild.RealmSlug,
		Region:  guild.Region,
		RealmID: guild.Integration.RealmID,
	}
	jsonBody, err := json.Marshal(body)
	if err != nil {
		fmt.Println("[wgt/progress] raider.io failed for", guild.Name, err)
		wg.Done()
		return
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonBody))
	if err != nil {
		fmt.Println("[wgt/progress] raider.io failed for", guild.Name, err)
		wg.Done()
		return
	}

	client := &http.Client{}
	_, err = client.Do(req)
	if err != nil {
		fmt.Println("[wgt/progress] raider.io failed for", guild.Name, err)
	}
	wg.Done()
}
