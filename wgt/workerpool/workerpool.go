package workerpool

import (
	"sync"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// GuildController - worker pool controller
type GuildController struct {
	Queue chan *configuration.Guild
	Done  chan *configuration.Guild
}

// New guild worker pool controller
func New() *GuildController {
	return &GuildController{}
}

// Allocate jobs
func (pool *GuildController) allocate(guilds []*configuration.Guild) {
	jobs := len(guilds)
	pool.Queue = make(chan *configuration.Guild, jobs)
	pool.Done = make(chan *configuration.Guild, jobs)

	for _, guild := range guilds {
		pool.Queue <- guild
	}
	close(pool.Queue)
}

// Run - configure and run worker pool
func (pool *GuildController) Run(workers int, guilds []*configuration.Guild, worker func(*sync.WaitGroup, chan *configuration.Guild, chan *configuration.Guild)) {
	var wg sync.WaitGroup
	pool.allocate(guilds)
	for i := 0; i < workers; i++ {
		wg.Add(1)
		go worker(&wg, pool.Queue, pool.Done)
	}
	wg.Wait()
	close(pool.Done)
}
