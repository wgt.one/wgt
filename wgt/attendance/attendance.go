package attendance

import (
	"fmt"
	"sync"

	"gitlab.com/wgt.one/wgt/wgt/attendance/export/api"
	"gitlab.com/wgt.one/wgt/wgt/attendance/export/html"
	"gitlab.com/wgt.one/wgt/wgt/attendance/report"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"gitlab.com/wgt.one/wgt/wgt/workerpool"
)

// prefix for logs
var prefix = "[wgt/attendance]"

// Export config
var Export configuration.Export

// RunPool - run attendance worker pool
func RunPool(workers int, guilds []*configuration.Guild) {
	fmt.Println(prefix, "starting pool")
	pool := workerpool.New()
	pool.Run(workers, guilds, Run)
}

//Run attendance
func Run(wg *sync.WaitGroup, queue chan *configuration.Guild, done chan *configuration.Guild) {
	defer wg.Done()
	api.Export = Export
	for guild := range queue {
		reports, err := getReports(guild)
		if err != nil {
			continue
		}
		err = api.ExportGuild(guild)
		if err != nil {
			fmt.Println(prefix, guild.Name, "api guild export failed", err)
		}
		err = api.ExportAttendance(guild)
		if err != nil {
			fmt.Println(prefix, guild.Name, "api export failed", err)
		}
		err = html.Export(&Export, guild, reports)
		if err != nil {
			fmt.Println(prefix, guild.Name, "html export failed", err)
			continue
		}
		fmt.Println(prefix, guild.Name, "done")
	}
}

func getReports(guild *configuration.Guild) ([]*report.Report, error) {
	reports := []*report.Report{}
	list, err := report.GetList(guild)
	if err != nil {
		fmt.Println(prefix, guild.Name, "failed", err)
		return reports, err
	}
	for _, wclListItem := range list {
		item, err := report.Parse(guild, wclListItem)
		if err != nil {
			if err.Error() != "report is empty after applied filters" {
				fmt.Println(prefix, guild.Name, "parse failed", err)
			}
			continue
		}
		reports = report.Append(reports, item)
	}
	// We count tries per boss by pulls on merged and cleaned reports
	for i, item := range reports {
		for idx, boss := range item.Bosses {
			reports[i].Bosses[idx].Tries = report.GetTriesByBoss(item, boss)
		}
	}

	return reports, nil
}
