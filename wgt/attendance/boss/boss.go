package boss

import (
	"gitlab.com/rakshazi/warcraftlogs-api"
	"gitlab.com/wgt.one/wgt/wgt/attendance/raider"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// Pull - info about any pull and raiders
type Pull struct {
	ID              int    // warcraftlogs report's pull id
	Name            string //boss name
	Kill            bool
	BossPercentage  int
	FightPercentage int
	Difficulty      string //boss difficulty
	Raiders         []string
}

// Boss information from parsed report
type Boss struct {
	Name       string
	Difficulty string
	Killed     bool
	Tries      int
}

// Parse bosses info and tries from report
func Parse(guild *configuration.Guild, report *warcraftlogs.Fight, reportID string) ([]*Boss, []*Pull) {
	var bosses []*Boss
	var pulls []*Pull
	for _, pull := range report.Fights {
		if isBoss(guild, pull) {
			bosses = appendMissing(bosses, &Boss{
				Name:       pull.Name,
				Difficulty: getDifficultyText(pull.Difficulty),
				Killed:     pull.Kill,
				Tries:      1,
			})
			pulls = append(pulls, &Pull{
				ID:              pull.Id,
				Name:            pull.Name,
				Kill:            pull.Kill,
				BossPercentage:  pull.BossPercentage,
				FightPercentage: pull.FightPercentage,
				Difficulty:      getDifficultyText(pull.Difficulty),
				Raiders:         raider.Parse(guild, report, reportID, pull.Id),
			})
		}
	}

	return bosses, pulls
}

// append boss if missing and increase tries counter on any attempt
func appendMissing(bosses []*Boss, boss *Boss) []*Boss {
	for i, item := range bosses {
		if item.Name == boss.Name && item.Difficulty == boss.Difficulty {
			bosses[i].Tries++
			if item.Killed == false && boss.Killed == true {
				bosses[i].Killed = true
			}
			return bosses
		}
	}
	return append(bosses, boss)
}

// Check if that pull is a boss that should be included
func isBoss(guild *configuration.Guild, pull *warcraftlogs.Pull) bool {
	// That's thrash
	if pull.Boss == 0 {
		return false
	}
	// That's ignored difficulty
	for _, ignoredDifficulty := range guild.Attendance.Ignore.Modes {
		if pull.Difficulty == int(ignoredDifficulty) {
			return false
		}
	}
	return true
}

// Convert WarcraftLogs Difficulty id to text
func getDifficultyText(difficulty int) string {
	switch difficulty {
	case 1:
		return "LFR"
	case 2:
		return "Flex"
	case 3:
		return "Normal"
	case 4:
		return "Heroic"
	case 5:
		return "Mythic"
	case 10:
		return "Challenge"
	}

	return "??"
}
