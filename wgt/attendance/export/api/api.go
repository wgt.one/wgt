package api

import (
	"bufio"
	"encoding/json"
	"os"
	"strings"

	"gitlab.com/wgt.one/wgt/wgt/attendance/export/file"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// Export config
var Export configuration.Export

// toMap - convert structure to interface{}
func toMap(input interface{}) map[string]interface{} {
	var output map[string]interface{}
	inputJSON, _ := json.Marshal(input)
	json.Unmarshal(inputJSON, &output)

	for key, value := range output {
		delete(output, key)
		key = strings.ToLower(key)
		output[key] = value
	}

	return output
}

// save data to file
func save(guild *configuration.Guild, filename string, ext string, data []byte) error {
	createAPIdir()
	path := Export.Path + "api/"
	fileHandler, err := file.Create(path, guild.Vanity, filename, ext)
	if err != nil {
		return err
	}
	defer fileHandler.Close()
	writer := bufio.NewWriter(fileHandler)
	_, err = writer.Write(data)
	if err != nil {
		return err
	}
	writer.Flush()

	return nil
}

// Create API dir
func createAPIdir() error {
	if _, err := os.Stat(Export.Path + "api/"); err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(Export.Path+"api/", 0755)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
