package api

import (
	"encoding/json"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// Attendance export type
type Attendance struct {
	Overall attendanceOverall   `json:"overall,omitempty"`
	Raiders []*attendanceRaider `json:"raiders,omitempty"`
	Raids   []*attendanceRaid   `json:"raids,omitempty"`
}

type attendanceOverall struct{}
type attendanceRaider struct {
	Name    string  `json:"name,omitmepty"`
	ByNight float64 `json:"byNight,omitempty"`
	ByPull  float64 `json:"byPull,omitempty"`
}
type attendanceRaid struct {
	ReportID string            `json:"reportId,omitempty"`
	Date     string            `json:"date,omitempty"`
	Pulls    []*attendancePull `json:"pulls,omitempty"`
}

type attendancePull struct {
	Boss  string `json:"boss,omitempty"`
	Pulls int64  `json:"pulls"`
}

// ExportGuild - export guild to json
func ExportGuild(guild *configuration.Guild) error {
	data := toMap(guild)
	delete(data, "id")
	delete(data, "mongoid")
	delete(data, "mongouserid")
	delete(data, "integration")
	dataJSON, _ := json.Marshal(data)
	err := save(guild, "guild", "json", dataJSON)
	if err != nil {
		return err
	}
	return nil
}

// ExportAttendance - export attendance to json
func ExportAttendance(guild *configuration.Guild) error {
	if !guild.Paid() {
		// Attendance export only for subscribers
		return nil
	}

	return nil
}
