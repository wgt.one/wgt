package html

import (
	"strconv"
	"text/template"

	"gitlab.com/wgt.one/wgt/wgt/attendance/boss"
	"gitlab.com/wgt.one/wgt/wgt/attendance/export/file"
	"gitlab.com/wgt.one/wgt/wgt/attendance/report"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// cell - HTML table cell
type cell struct {
	Text      string
	ReportID  string
	Character string
}

// raid - per-raid attendance
type raid struct {
	ReportID   string
	Title      string
	Path       string
	Bosses     []*boss.Boss
	Attendance map[string]map[string]int
}

// Export HTML attendance
func Export(config *configuration.Export, guild *configuration.Guild, reports []*report.Report) error {
	showTable := true
	showHistory := true
	characters := report.GetAllCharacters(reports)
	headers := getHeaders(reports)
	table := getTable(guild, characters, reports)
	history := getHistory(reports)
	if len(table) == 0 {
		showTable = false
	}
	if len(history) == 0 {
		showHistory = false
	}

	vars := struct {
		Title       string
		UpdatedAt   string
		Guild       *configuration.Guild
		GuildID     string
		Headers     []string
		Table       [][]*cell
		History     []*raid
		ShowTable   bool
		ShowHistory bool
	}{
		Title:       guild.Name,
		UpdatedAt:   "{{ site.time | date_to_rfc822 }}", // otherwise wgt will fail with jekyll template
		Guild:       guild,
		GuildID:     guild.MongoID.Hex(),
		Headers:     headers,
		Table:       table,
		History:     history,
		ShowTable:   showTable,
		ShowHistory: showHistory,
	}
	err := save(config.Path, guild.Vanity, config.Template.Attendance, "index", vars)
	if err != nil {
		return err
	}
	err = save(config.Path, guild.Vanity, config.Template.History, "history", vars)
	return err
}

func save(dir string, vanity string, templateFile string, filename string, vars interface{}) error {
	tpl, err := template.ParseFiles(templateFile)
	if err != nil {
		return err
	}

	fileHandler, err := file.Create(dir, vanity, filename, "html")
	if err != nil {
		return err
	}

	defer fileHandler.Close()
	err = tpl.Execute(fileHandler, vars)
	if err != nil {
		return err
	}

	return nil
}

// getHeaders - get table headers
func getHeaders(reports []*report.Report) []string {
	headers := []string{"Character", "Att. by Night", "Att. by Pull"}

	for _, report := range reports {
		headers = append(headers, report.Date.Format("02/01"))
	}

	return headers
}

// get table matrix
func getTable(guild *configuration.Guild, characters []string, reports []*report.Report) [][]*cell {
	var data [][]*cell
	for _, character := range characters {
		byNight, byPull := report.GetCharacterAttendance(character, reports)
		if byNight < guild.Attendance.Ignore.Threshold {
			continue
		}
		line := make([]*cell, len(reports)+3)
		line[0] = &cell{ReportID: "", Character: "", Text: character}
		line[1] = &cell{ReportID: "", Character: "", Text: strconv.FormatFloat(byNight, 'f', 2, 64)}
		line[2] = &cell{ReportID: "", Character: "", Text: strconv.FormatFloat(byPull, 'f', 2, 64)}
		for i, item := range reports {
			cell := &cell{Character: character, ReportID: item.ID, Text: ""}
			if report.HasCharacter(character, item) {
				cell.Text = strconv.Itoa(report.GetPulls(character, item)) + "/" + strconv.Itoa(len(item.Pulls))
			}
			line[i+3] = cell
		}
		data = append(data, line)
	}

	return sortTable(data, 1)
}

// getHistory - get per-raid attendance
func getHistory(reports []*report.Report) []*raid {
	var data []*raid
	for _, item := range reports {
		data = append(data, &raid{
			Title:      item.Date.Format("02/01"),
			Path:       item.Date.Format("02-01"),
			ReportID:   item.ID,
			Bosses:     item.Bosses,
			Attendance: report.GetAttendance(item),
		})
	}

	return data
}
