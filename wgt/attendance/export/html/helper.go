package html

import (
	"sort"
	"strconv"
)

// Sort table line by index
func sortTable(table [][]*cell, index int) [][]*cell {
	sort.Slice(table, func(i, j int) bool {
		// We want to sort by attendance, that's float
		iItem, _ := strconv.ParseFloat(table[i][index].Text, 64)
		jItem, _ := strconv.ParseFloat(table[j][index].Text, 64)
		return iItem > jItem
	})
	return table
}
