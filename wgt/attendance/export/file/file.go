package file

import (
	"os"
	"path/filepath"

	"github.com/gosimple/slug"
)

// Create new file with subdir
// Example: Create(source/, retail-guild-realm-eu, index, html) -> source/retail-guild-realm-eu/index.html
func Create(path string, dir string, filename string, ext string) (*os.File, error) {
	if _, err := os.Stat(path + slug.Make(dir)); err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(path+slug.Make(dir), 0755)
			if err != nil {
				return nil, err
			}
		}
	}

	filename = filepath.Join(path, slug.Make(dir), slug.Make(filename)+"."+ext)
	file, err := os.Create(filename)
	if err != nil {
		return nil, err
	}

	return file, nil
}
