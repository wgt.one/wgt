package raider

import (
	"gitlab.com/rakshazi/warcraftlogs-api"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// Parse raiders from report
func Parse(guild *configuration.Guild, wclReport *warcraftlogs.Fight, reportID string, pullID int) []string {
	raiders := []string{}
	if pullID == 0 {
		raiders = GetBenched(guild, reportID)
	}
	for _, raider := range wclReport.Friendlies {
		if shouldInclude(raider, pullID) {
			character := match(guild, raider.Name)
			raiders = AppendMissing(raiders, character)
		}
	}
	if guild.Attendance.Ignore.NonGuild {
		raiders = removePUGs(guild, raiders)
	}

	return raiders
}

// Match alt/twink, return main char name if found
func match(guild *configuration.Guild, character string) string {
	for _, item := range guild.Alts {
		if item.Main == character {
			return item.Main
		}
		for _, alt := range item.Alts {
			if alt == character {
				return item.Main
			}
		}
	}

	return character
}

// If pullID filter is set, check raider fights
func shouldInclude(raider *warcraftlogs.Raider, pullID int) bool {
	if raider.Server == "" || raider.Type == "Unknown" || raider.Type == "" {
		return false
	}

	if pullID == 0 {
		return true
	}
	for _, fight := range raider.Fights {
		if pullID == fight.Id {
			return true
		}
	}
	return false
}

// AppendMissing character to slice only if not exists
func AppendMissing(characters []string, character string) []string {
	if isExists(characters, character) {
		return characters
	}

	return append(characters, character)
}

// Check if character exists in slice
func isExists(characters []string, character string) bool {
	for _, raider := range characters {
		if character == raider {
			return true
		}
	}
	return false
}

// Check if character is guild member
func isGuildMember(guild *configuration.Guild, character string) bool {
	for _, member := range guild.Members {
		if member.Name == character {
			return true
		}
	}
	return false
}

// Remove non-guild/PUGs from slice
func removePUGs(guild *configuration.Guild, characters []string) []string {
	for _, character := range characters {
		if !isGuildMember(guild, character) {
			characters = remove(characters, character)
		}
	}
	return characters
}

// Remove character from slice
func remove(characters []string, character string) []string {
	for idx, item := range characters {
		if item == character {
			characters[idx] = characters[len(characters)-1]
			return characters[:len(characters)-1]
		}
	}
	return characters
}
