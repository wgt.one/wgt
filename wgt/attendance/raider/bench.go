package raider

import (
	"fmt"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"gitlab.com/wgt.one/wgt/wgt/mongodb"
)

// GetBenched - get raiders on bench
func GetBenched(guild *configuration.Guild, reportID string) []string {
	characters := []string{}
	benches, err := mongodb.GetBenches(guild, reportID)
	if err != nil {
		fmt.Println("[wgt/attendance] get characters on bench", guild.Name, err)
		return characters
	}
	for _, bench := range benches {
		characters = AppendMissing(characters, bench.Character)
	}
	characters = appendAutoBenched(guild, reportID, characters)
	return characters
}

// appendAutoBenched - if character set as "auto bench", append it to list of raiders
// if he is missing in report
func appendAutoBenched(guild *configuration.Guild, reportID string, raiders []string) []string {
	for _, raider := range guild.Attendance.Bench {
		if isExists(raiders, raider) {
			continue
		}
		// That function called after GetCharactersOnBench, so we assume that manual "on the bench" chars
		// already included in report, in that case we will add missing characters.
		err := mongodb.AddBench(guild, reportID, raider)
		if err != nil {
			fmt.Println("[wgt/attendance] create bench", guild.Name, err)
			return raiders
		}
		raiders = append(raiders, raider)
	}
	return raiders
}
