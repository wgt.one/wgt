package report

// GetAllCharacters from reports slice
func GetAllCharacters(reports []*Report) []string {
	characters := []string{}
	added := map[string]bool{}

	for _, item := range reports {
		for _, character := range item.Raiders {
			if !added[character] {
				characters = append(characters, character)
				added[character] = true
			}
		}
	}
	return characters
}

// HasCharacter - check if report contains character
func HasCharacter(character string, report *Report) bool {
	for _, char := range report.Raiders {
		if char == character {
			return true
		}
	}

	return false
}

// GetCharacterAttendance - get character's attendance by pull and by night
func GetCharacterAttendance(character string, reports []*Report) (float64, float64) {
	all := len(reports)
	pulls := 0
	allPulls := 0
	present := 0
	for _, report := range reports {
		for _, raider := range report.Raiders {
			if raider == character {
				present++
			}
		}
		pulls += GetPulls(character, report)
		allPulls += len(report.Pulls)
	}
	return (float64(present) / float64(all)) * 100, (float64(pulls) / float64(allPulls)) * 100
}

// GetAttendance - get attendance table per report
func GetAttendance(report *Report) map[string]map[string]int {
	attendance := make(map[string]map[string]int)
	pullsCount := len(report.Pulls)
	for _, pull := range report.Pulls {
		for _, character := range pull.Raiders {
			if _, ok := attendance[character]; !ok {
				attendance[character] = make(map[string]int, pullsCount)
			}
			attendance[character][pull.Name]++
		}
	}

	return attendance
}

// GetPulls - Get count of character pulls on raid
func GetPulls(character string, report *Report) int {
	count := 0
	for _, pull := range report.Pulls {
		for _, name := range pull.Raiders {
			if name == character {
				count++
			}
		}
	}

	return count
}
