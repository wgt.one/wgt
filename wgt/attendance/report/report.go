package report

import (
	"fmt"
	"time"

	"gitlab.com/rakshazi/warcraftlogs-api"
	"gitlab.com/wgt.one/wgt/3rdparty/wcl"
	"gitlab.com/wgt.one/wgt/wgt/attendance/boss"
	"gitlab.com/wgt.one/wgt/wgt/attendance/raider"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// Report - processed data from WarcraftLogs report, fights and matched raiders
type Report struct {
	ID      string
	Date    time.Time
	Raiders []string
	Bosses  []*boss.Boss
	Pulls   []*boss.Pull
}

// Parse WarcraftLogs report
func Parse(guild *configuration.Guild, listItem *warcraftlogs.Report) (*Report, error) {
	report := &Report{
		ID:   listItem.Id,
		Date: time.Unix(*listItem.StartTime/1000, 0),
	}
	wclReport, err := download(guild.Game, listItem.Id)
	if err != nil {
		return report, err
	}
	report.Raiders = raider.Parse(guild, &wclReport, listItem.Id, 0)
	report.Bosses, report.Pulls = boss.Parse(guild, &wclReport, listItem.Id)
	if len(report.Bosses) == 0 {
		err = fmt.Errorf("report is empty after applied filters")
		return report, err
	}

	return report, nil
}

// Append and merge reports
func Append(reports []*Report, report *Report) []*Report {
	merged := false
	for idx, item := range reports {
		if item.Date.Format("02/01") != report.Date.Format("02/01") {
			continue
		}
		reports[idx] = merge(item, report)
		merged = true
	}
	if !merged {
		reports = append(reports, report)
	}

	return reports
}

// Merge reports
func merge(base *Report, item *Report) *Report {
	// raiders
	for _, character := range item.Raiders {
		base.Raiders = raider.AppendMissing(base.Raiders, character)
	}
	base = mergeBosses(base, item)
	return mergePulls(base, item)
}

func mergeBosses(base *Report, item *Report) *Report {
	for _, itemBoss := range item.Bosses {
		idx := hasBossPos(base, itemBoss)
		if idx > -1 { // if boss exists in base report
			// set killed
			if !base.Bosses[idx].Killed && itemBoss.Killed {
				base.Bosses[idx].Killed = true
			}
		} else { // if boss does NOT exist in base report
			base.Bosses = append(base.Bosses, itemBoss)
		}
	}

	return base
}

func mergePulls(base *Report, item *Report) *Report {
	for _, itemPull := range item.Pulls {
		idx := hasPullPos(base, itemPull)
		if idx > -1 { // if pull exists in base report
			// Merge raiders
			for _, character := range itemPull.Raiders {
				base.Pulls[idx].Raiders = raider.AppendMissing(base.Pulls[idx].Raiders, character)
			}

		} else { // if pull does NOT exists in base report
			base.Pulls = append(base.Pulls, itemPull)
		}
	}

	return base
}

func hasBossPos(report *Report, item *boss.Boss) int {
	for idx, boss := range report.Bosses {
		if boss.Name == item.Name && boss.Difficulty == item.Difficulty {
			return idx
		}
	}

	return -1
}

func hasPullPos(report *Report, item *boss.Pull) int {
	for idx, pull := range report.Pulls {
		if pull.Name == item.Name &&
			pull.Difficulty == item.Difficulty &&
			pull.BossPercentage == item.BossPercentage &&
			pull.FightPercentage == item.FightPercentage &&
			pull.Kill == item.Kill {
			return idx
		}
	}

	return -1
}

// GetTriesByBoss - Get count of tries on selected boss
func GetTriesByBoss(report *Report, item *boss.Boss) int {
	tries := 0
	for _, pull := range report.Pulls {
		if item.Name == pull.Name &&
			item.Difficulty == pull.Difficulty {
			tries++
		}
	}

	return tries
}

// Get actual report content
func download(game string, code string) (warcraftlogs.Fight, error) {
	report, err := wcl.V1[game].Fights(code)

	return report, err
}
