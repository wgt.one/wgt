package report

import (
	"math"
	"sort"
	"time"

	"gitlab.com/rakshazi/warcraftlogs-api"
	"gitlab.com/wgt.one/wgt/3rdparty/wcl"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
)

// GetList - get list of Warcraft Logs reports ids
func GetList(guild *configuration.Guild) ([]*warcraftlogs.Report, error) {
	list, err := wcl.V1[guild.Game].ReportsForGuild(guild.Name, guild.Realm, guild.Region)
	list = applyIgnoreFilters(guild, list)
	// Sort list by date
	sort.Slice(list, func(i, j int) bool {
		iDate := time.Unix(*list[i].StartTime/1000, 0)
		jDate := time.Unix(*list[j].StartTime/1000, 0)
		return iDate.After(jDate)
	})
	return list, err
}

// Apply Guild's ignore filters on reports list
func applyIgnoreFilters(guild *configuration.Guild, list []*warcraftlogs.Report) []*warcraftlogs.Report {
	clean := []*warcraftlogs.Report{}
	for _, report := range list {
		if shouldInclude(guild, report) {
			clean = append(clean, report)
		}
	}

	// Shrink reports to 1.2x size of maxRaids config (because some of reports will be merged later)
	size := int(math.Round(float64(1.2) * float64(guild.Attendance.MaxReports)))
	if len(clean) > size {
		clean = append([]*warcraftlogs.Report{}, clean[:size]...)
	}

	return clean
}

// Check if report from list should be processed further
func shouldInclude(guild *configuration.Guild, report *warcraftlogs.Report) bool {
	date := time.Unix(*report.StartTime/1000, 0)
	// Ignore reports before selected date
	if guild.Attendance.Ignore.Date != "" {
		startDate, _ := time.Parse("2006-01-02", guild.Attendance.Ignore.Date)
		if date.Before(startDate) {
			return false
		}
	}
	// Ignore reports by day
	if len(guild.Attendance.Ignore.Days) > 0 {
		for _, day := range guild.Attendance.Ignore.Days {
			if int64(date.Weekday()) == day {
				return false
			}
		}
	}
	// Ignore reports by raid
	if len(guild.Attendance.Raids) == 0 || isRaidAllowed(report.Zone, guild.Attendance.Raids) {
		return true
	}

	return true
}

func isRaidAllowed(raid int64, allowed []int64) bool {
	for _, raidID := range allowed {
		if raidID == raid {
			return true
		}
	}

	return false
}
