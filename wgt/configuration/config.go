package configuration

// Config - System config
type Config struct {
	Mongo        Mongo        `json:"mongo,omitempty"`
	Export       Export       `json:"export,omitempty"`
	Blizzard     Blizzard     `json:"blizzard,omitempty"`
	WarcraftLogs WarcraftLogs `json:"warcraftlogs,omitempty"`
}

// Mongo DB config
type Mongo struct {
	URL               string `json:"url"`
	User              string `json:"user"`
	Database          string `json:"database"`
	Protocol          string `json:"protocol"`
	Password          string `json:"password"`
	Collection        string `json:"collection"`
	CollectionBenches string `json:"collection_benches"`
}

// WarcraftLogs API config
type WarcraftLogs struct {
	Key          string `json:"key,omitempty"`
	ClientID     string `json:"client_id,omitempty"`
	ClientSecret string `json:"client_secret"`
}

// Blizzard API config
type Blizzard struct {
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

// Export configuration
type Export struct {
	Path     string `json:"path,omitempty"`
	Template struct {
		Attendance string `json:"attendance,omitempty"`
		History    string `json:"history,omitempty"`
	} `json:"template,omitempty"`
}
