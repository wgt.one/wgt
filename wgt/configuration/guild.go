package configuration

import (
	"strings"

	"gitlab.com/wgt.one/wgt/3rdparty/blizzard"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Guild represents configuration set for 1 guild
type Guild struct {
	ID          string
	MongoID     primitive.ObjectID `bson:"_id,omitempty"`
	MongoUserID string             `bson:"user_id,omitempty"`
	Game        string             `json:"game,omitempty"`
	RealmSlug   string
	Progress    map[string]string
	Alts        []Alt                   `bson:"alts,omitempty"`
	Name        string                  `bson:"name"`
	Vanity      string                  `bson:"vanity,omitempty"` // Vanity/Custom URL/file
	Realm       string                  `bson:"realm"`
	Region      string                  `bson:"region"`
	Attendance  Attendance              `bson:"attendance,omitempty"`
	Integration Integration             `bson:"integration,omitempty"`
	Members     []*blizzard.GuildMember `bson:"members,omitempty"`
}

// Integration with Raider.io and WarcraftLogs
type Integration struct {
	Key          string       `bson:"apikey,omitempty"` // Only for backward compatibility
	WarcraftLogs WarcraftLogs `bson:"warcraftlogs,omitempty"`
	Blizzard     Blizzard     `bson:"blizzard,omitempty"`
	RealmID      int          `bson:"realmId,omitempty"`
	GuildID      int          `bson:"guildId,omitempty"`
}

// Attendance configuration for guild
type Attendance struct {
	MaxReports int     `bson:"maxReports,default:10"`
	Raids      []int64 `bson:"raids,omitempty"` // 26 - Castle Nathria,  1000x - classic
	Bench      []string
	Ignore     struct {
		Threshold float64 `bson:"threshold,omitempty"` // Ignore chars with attendance lower than that number
		Modes     []int64 `bson:"modes,omitempty"`     // Ignore bosses difficulty
		Days      []int64 `bson:"days,omitempty"`      // Ignore reports, recored in following days. 0 - Sunday..
		Date      string  `bson:"date,omitempty"`      //Ignore reports before that date
		NonGuild  bool    `bson:"nonguild,omitempty"`
	}
}

// Alt represents list of all characters of one player
type Alt struct {
	Main string   `bson:"main"`
	Alts []string `bson:"alts"`
}

// Bench - is a state when player was online on RT, but not in raid group
type Bench struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	UserID    string             `bson:"user_id,omitempty"`
	GuildID   string             `bson:"guild_id,omitempty"`
	ReportID  string             `bson:"report_id,omitempty"`
	Character string             `bson:"character,omitempty"`
}

// WL2slug - Pairs of (left) WarcraftLogs' realm slugs and (right) standard slugs
var WL2slug = map[string]string{
	"ясеневыи-лес":     "ashenvale",
	"азурегос":         "azuregos",
	"черныи-шрам":      "blackscar",
	"пиратская-бухта":  "booty-bay",
	"бореиская-тундра": "borean-tundra",
	"страж-смерти":     "deathguard",
	"ткач-смерти":      "deathweaver",
	"подземье":         "deepholm",
	"вечная-песня":     "eversong",
	"дракономор":       "fordragon",
	"галакронд":        "galakrond",
	"голдринн":         "goldrinn",
	"гордунни":         "gordunni",
	"седогрив":         "greymane",
	"гром":             "grom",
	"ревущии-фьорд":    "howling-fjord",
	"король-лич":       "lich-king",
	"разувии":          "razuvious",
	"свежеватель-душ":  "soulflayer",
	"термоштепсель":    "thermaplugg",
}

// Paid - check if guild is patreon subscriber
func (guild *Guild) Paid() bool {
	// New guilds
	if guild.Vanity == "" {
		return false
	}

	// Classic guilds
	if strings.HasPrefix(guild.Vanity, "classic-") {
		return false
	}

	// Retail guilds
	if strings.HasPrefix(guild.Vanity, "retail-") {
		return false
	}

	return true
}
