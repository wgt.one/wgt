package configuration

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
)

// LoadEnv - load config from env vars
func LoadEnv(config *Config) {
	// WarcraftLogs
	if os.Getenv("WGT_WARCRAFTLOGS_KEY") != "" {
		config.WarcraftLogs.Key = os.Getenv("WGT_WARCRAFTLOGS_KEY")
	}

	if os.Getenv("WGT_WARCRAFTLOGS_CLIENT_ID") != "" {
		config.WarcraftLogs.ClientID = os.Getenv("WGT_WARCRAFTLOGS_CLIENT_ID")
	}

	if os.Getenv("WGT_WARCRAFTLOGS_CLIENT_SECRET") != "" {
		config.WarcraftLogs.ClientSecret = os.Getenv("WGT_WARCRAFTLOGS_CLIENT_SECRET")
	}

	// MongoDB
	if os.Getenv("WGT_MONGO_URL") != "" {
		config.Mongo.URL = os.Getenv("WGT_MONGO_URL")
		config.Mongo.User = os.Getenv("WGT_MONGO_USER")
		config.Mongo.Protocol = os.Getenv("WGT_MONGO_PROTOCOL")
		config.Mongo.Password = url.QueryEscape(os.Getenv("WGT_MONGO_PASSWORD"))
		config.Mongo.Database = os.Getenv("WGT_MONGO_DB")
		config.Mongo.Collection = os.Getenv("WGT_MONGO_COLLECTION")
		config.Mongo.CollectionBenches = os.Getenv("WGT_MONGO_COLLECTION_BENCHES")
	}

	// Blizzard
	if os.Getenv("WGT_BLIZZARD_CLIENT_ID") != "" {
		config.Blizzard.ClientID = os.Getenv("WGT_BLIZZARD_CLIENT_ID")
		config.Blizzard.ClientSecret = os.Getenv("WGT_BLIZZARD_CLIENT_SECRET")
	}

	// Export
	if os.Getenv("WGT_EXPORT_PATH") != "" {
		config.Export.Path = os.Getenv("WGT_EXPORT_PATH")
	}

	if os.Getenv("WGT_EXPORT_TEMPLATE_ATTENDANCE") != "" {
		config.Export.Template.Attendance = os.Getenv("WGT_EXPORT_TEMPLATE_ATTENDANCE")
	}

	if os.Getenv("WGT_EXPORT_TEMPLATE_HISTORY") != "" {
		config.Export.Template.History = os.Getenv("WGT_EXPORT_TEMPLATE_HISTORY")
	}
}

// Read config
func Read(file string) Config {
	config := Config{}
	if _, err := os.Stat(file); os.IsNotExist(err) {
		fmt.Println("[wgt/configuration] file not found, using only env config")
		LoadEnv(&config)
		return config
	}
	data, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		panic(err)
	}

	LoadEnv(&config)
	fmt.Println("[wgt/configuration] loaded")
	return config
}
