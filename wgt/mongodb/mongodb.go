package mongodb

import (
	"context"
	"time"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var database *mongo.Database

// Connect - connect to database and select collection
func Connect(config *configuration.Mongo) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	uri := config.Protocol + "://" + config.User + ":" + config.Password + "@" + config.URL + "/test?retryWrites=true&w=majority"
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		return err
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return err
	}
	database = client.Database(config.Database)
	return nil
}
