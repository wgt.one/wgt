package mongodb

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"go.mongodb.org/mongo-driver/bson"
)

// GetGuilds - get all guilds info from collection
func GetGuilds(config *configuration.Config) ([]*configuration.Guild, error) {
	fmt.Println("[wgt/mongodb] loading guilds")
	guilds := []*configuration.Guild{}
	collection := database.Collection("guilds")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	cursor, err := collection.Find(ctx, bson.D{})
	if err != nil {
		return guilds, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var item configuration.Guild
		err := cursor.Decode(&item)
		if err != nil {
			fmt.Println("[wgt/mongodb]", err)
			continue
		}
		guilds = append(guilds, &item)
	}
	return guilds, nil
}

// UpdateGuildsVanity - check if guild in DB has no vanity/slug URL and set it
func UpdateGuildsVanity(guild *configuration.Guild) error {
	var err error
	collection := database.Collection("guilds")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": guild.MongoID}}
	update := bson.M{"$set": bson.M{"vanity": guild.Vanity}}
	_, err = collection.UpdateOne(ctx, filter, update)
	return err
}

// UpdateGuildIntegrationRealmID - set guild.integration.realmId
func UpdateGuildIntegrationRealmID(guild *configuration.Guild, realmID int) error {
	collection := database.Collection("guilds")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": guild.MongoID}}
	update := bson.M{"$set": bson.M{"integration.realmId": realmID}}
	_, err := collection.UpdateOne(ctx, filter, update)
	return err
}

// UpdateGuildIntegrationGuildID - set guild.integration.guildId
func UpdateGuildIntegrationGuildID(guild *configuration.Guild, guildID int) error {
	collection := database.Collection("guilds")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": guild.MongoID}}
	update := bson.M{"$set": bson.M{"integration.guildId": guildID}}
	_, err := collection.UpdateOne(ctx, filter, update)
	return err
}
