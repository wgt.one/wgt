package mongodb

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// AddBench - add "on the bench" mark for selected guild, report, character
func AddBench(guild *configuration.Guild, reportID string, character string) error {
	var err error
	collection := database.Collection("benches")
	bench := &configuration.Bench{
		UserID:    guild.MongoUserID,
		GuildID:   guild.MongoID.Hex(),
		ReportID:  reportID,
		Character: character,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	filter := bson.M{
		"user_id":   bson.M{"$eq": bench.UserID},
		"guild_id":  bson.M{"$eq": bench.GuildID},
		"report_id": bson.M{"$eq": bench.ReportID},
		"character": bson.M{"$eq": bench.Character},
	}
	update := bson.M{
		"$set":         bench,
		"$setOnInsert": bson.M{"status": "active"},
	}
	opts := options.Update().SetUpsert(true)
	_, err = collection.UpdateOne(ctx, filter, update, opts)
	return err
}

// GetBenches - Get all "on the bench" characters from db
func GetBenches(guild *configuration.Guild, reportID string) ([]*configuration.Bench, error) {
	collection := database.Collection("benches")
	data := []*configuration.Bench{}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	query := bson.M{
		"$or": bson.A{
			bson.M{
				"report_id": bson.M{"$eq": reportID},
				"guild_id":  bson.M{"$eq": guild.MongoID},
				"user_id":   bson.M{"$eq": guild.MongoUserID},
				"status":    "active", // Autobench feature must have list of all enabled/disabled benches or it will re-add them.
			},
			bson.M{
				"report_id": bson.M{"$eq": reportID},
				"guild_id":  bson.M{"$eq": guild.MongoID.Hex()},
				"user_id":   bson.M{"$eq": guild.MongoUserID},
				"status":    "active", // Autobench feature must have list of all enabled/disabled benches or it will re-add them.
			},
		},
	}
	cursor, err := collection.Find(ctx, query)
	if err != nil {
		return data, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var item configuration.Bench
		err := cursor.Decode(&item)
		if err != nil {
			fmt.Println("[wgt/mongodb]: GetBenches", err)
			continue
		}
		data = append(data, &item)
	}
	return data, nil
}
