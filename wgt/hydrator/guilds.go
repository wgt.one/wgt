package hydrator

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/gosimple/slug"
	"gitlab.com/wgt.one/wgt/3rdparty/blizzard"
	"gitlab.com/wgt.one/wgt/3rdparty/wcl"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"gitlab.com/wgt.one/wgt/wgt/mongodb"
	"gitlab.com/wgt.one/wgt/wgt/workerpool"
)

// Guilds - hydrate guilds and remove invalid guilds
func Guilds(workers int, slice []*configuration.Guild) []*configuration.Guild {
	fmt.Println("[wgt/hydrator] hydrating guilds")
	guilds := []*configuration.Guild{}
	pool := workerpool.New()
	pool.Run(workers, slice, gWorker)
	for guild := range pool.Done {
		if guild.ID != "" {
			guilds = append(guilds, guild)
		}
	}

	return guilds
}

func gWorker(wg *sync.WaitGroup, queue chan *configuration.Guild, done chan *configuration.Guild) {
	defer wg.Done()
	for guild := range queue {
		HydrateGuild(guild)
		IncludeProgress(guild)
		done <- guild
	}
}

// HydrateGuild a guild
func HydrateGuild(guild *configuration.Guild) {
	if guild.Game == "" {
		guild.Game = "retail"
	}
	guild.RealmSlug = normalizeRealm(guild.Realm)
	guild.ID = guild.Game + "/" + guild.Name + "/" + guild.RealmSlug + "/" + guild.Region

	if guild.Vanity == "" {
		guild.Vanity = slug.Make(guild.ID)
	}

	if guild.Integration.RealmID <= 0 {
		realmID, err := getRealmID(guild)
		if err == nil {
			mongodb.UpdateGuildIntegrationRealmID(guild, realmID)
		}
	}

	if guild.Integration.GuildID <= 0 {
		guildID, err := wcl.GetGuildID(guild)
		if err != nil {
			// in 99% of cases that means guild not found on WarcaftLogs
			return
		}
		mongodb.UpdateGuildIntegrationGuildID(guild, guildID)
	}

	if guild.Game == "retail" {
		roster, err := blizzard.GetRoster(guild.Name, guild.RealmSlug, guild.Region)
		if err != nil {
			fmt.Println("[3rdparty/blizzard] cannot get roster of", guild.ID, err)
			return
		}
		guild.Members = roster
	}
}

// normalizeRealm convert very fcking special WarcraftLogs ream slug to standard realm slug
func normalizeRealm(realm string) string {
	if _, ok := configuration.WL2slug[realm]; !ok {
		return realm
	}

	return configuration.WL2slug[realm]
}

// Get Raider.IO realm ID
func getRealmID(guild *configuration.Guild) (int, error) {
	type raiderioGuildDetailsResponse struct {
		GuildDetails struct {
			Guild struct {
				Realm struct {
					ID int `json:"id"`
				} `json:"realm"`
			} `json:"guild"`
		} `json:"guildDetails"`
	}
	guildPage := "https://raider.io/api/guilds/details?region=" + guild.Region + "&realm=" + guild.RealmSlug + "&guild=" + url.QueryEscape(guild.Name)
	resp, err := http.Get(guildPage)
	if err != nil {
		return 0, err
	}
	if resp.StatusCode != 200 {
		return 0, fmt.Errorf("API returned HTTP %v", resp.StatusCode)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	var response raiderioGuildDetailsResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return 0, err
	}
	return response.GuildDetails.Guild.Realm.ID, nil
}

// IncludeProgress - add raid progression to guild object
func IncludeProgress(guild *configuration.Guild) error {
	// only retail is supported
	if guild.Game != "retail" {
		return nil
	}
	progress, err := getGuildRaidProgression(guild)
	if err != nil {
		return err
	}
	guild.Progress = progress
	return nil
}

// get guild raid progress from Raider.io API
func getGuildRaidProgression(guild *configuration.Guild) (map[string]string, error) {
	type RaiderIORaidProgressionResponse struct {
		Progress map[string]struct{ Summary string } `json:"raid_progression"`
	}
	var rioAPI = "https://raider.io/api/v1/"
	var client = http.Client{
		Timeout: time.Second * 10,
	}
	urlString := fmt.Sprintf(
		"%s%s?region=%s&realm=%s&name=%s&fields=raid_progression",
		rioAPI,
		"guilds/profile",
		guild.Region,
		guild.RealmSlug,
		url.PathEscape(guild.Name))
	req, err := http.NewRequest("GET", urlString, nil)

	if err != nil {
		return nil, err
	}

	res, getErr := client.Do(req)
	if getErr != nil {
		return nil, getErr
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return nil, readErr
	}

	responseHolder := &RaiderIORaidProgressionResponse{}
	err = json.Unmarshal(body, responseHolder)
	if err != nil {
		return nil, err
	}
	progress := make(map[string]string, len(responseHolder.Progress))
	for name, raid := range responseHolder.Progress {
		progress[name] = raid.Summary
	}

	return progress, nil
}
