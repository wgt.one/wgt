package hydrator

import (
	"fmt"
	"sync"

	"gitlab.com/wgt.one/wgt/3rdparty/blizzard/oauth2"
	"gitlab.com/wgt.one/wgt/3rdparty/wcl"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"gitlab.com/wgt.one/wgt/wgt/workerpool"
)

// RunCredentialsPool - self-documented
func RunCredentialsPool(workers int, guilds []*configuration.Guild) ([]string, []*configuration.Blizzard) {
	wclKeys := []string{}
	blizzardCredentials := []*configuration.Blizzard{}
	fmt.Println("[wgt/hydrator] starting credentials pool")
	pool := workerpool.New()
	pool.Run(workers, guilds, cWorker)
	for guild := range pool.Done {
		if guild.Integration.WarcraftLogs.Key != "" {
			wclKeys = append(wclKeys, guild.Integration.WarcraftLogs.Key)
		}
		if guild.Integration.Blizzard.ClientID != "" {
			blizzardCredentials = append(blizzardCredentials, &guild.Integration.Blizzard)
		}
	}
	return wclKeys, blizzardCredentials
}

func cWorker(wg *sync.WaitGroup, queue chan *configuration.Guild, done chan *configuration.Guild) {
	defer wg.Done()
	for guild := range queue {
		//Unset WarcraftLogs APIv1 key if it's not valid
		wclKey := guild.Integration.WarcraftLogs.Key
		if !wcl.ValidKey(wclKey) {
			guild.Integration.WarcraftLogs.Key = ""
		}
		//Unset Blizzard API credentials if they are not valid
		blizzardCredential := guild.Integration.Blizzard
		_, blizzardError := oauth2.GenerateToken(blizzardCredential.ClientID, blizzardCredential.ClientSecret)
		if blizzardError != nil {
			guild.Integration.Blizzard = configuration.Blizzard{}
		}
		done <- guild
	}
}
