package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/wgt.one/wgt/3rdparty/blizzard"
	"gitlab.com/wgt.one/wgt/3rdparty/blizzard/oauth2"
	"gitlab.com/wgt.one/wgt/3rdparty/wcl"
	"gitlab.com/wgt.one/wgt/wgt/attendance"
	"gitlab.com/wgt.one/wgt/wgt/configuration"
	"gitlab.com/wgt.one/wgt/wgt/hydrator"
	"gitlab.com/wgt.one/wgt/wgt/mongodb"
	"gitlab.com/wgt.one/wgt/wgt/progress"
)

// WGT Config
var config configuration.Config
var arguments []string

// Workers/threads per pool
const threads = 5

func main() {
	config = initSystem()
	guilds, err := mongodb.GetGuilds(&config)

	if err != nil {
		panic(err)
	}
	switch arguments[1] {
	case "progress":
		progress.RunPool(threads, guilds)
	case "attendance":
		guilds = hydrateGuilds(guilds)
		attendance.Export = config.Export
		attendance.RunPool(threads, guilds)
	default:
		guilds = hydrateGuilds(guilds)
		progress.RunPool(threads, guilds)
		attendance.Export = config.Export
		attendance.RunPool(threads, guilds)
	}
	fmt.Println("[wgt] done")
}

// Set credentials and hydrate guilds
func hydrateGuilds(guilds []*configuration.Guild) []*configuration.Guild {
	// Get guilds' keys and credentials and use them in shared pools
	wclKeys, blizzardCredentials := hydrator.RunCredentialsPool(threads, guilds)
	wcl.V1["classic"].SetTokens(wclKeys)
	wcl.V1["retail"].SetTokens(wclKeys)

	credentials := []oauth2.Credential{}
	for _, item := range blizzardCredentials {
		credentials = append(credentials, oauth2.Credential{ID: item.ClientID, Secret: item.ClientSecret})
	}
	blizzard.Init(credentials)
	// Prepare guilds
	return hydrator.Guilds(threads, guilds)
}

func initSystem() configuration.Config {
	// Command
	arguments = os.Args
	if len(arguments) == 1 {
		arguments = append(arguments, "all")
	}

	// System config
	file := flag.String("config", "wgt.json", "Path to the configuration file")
	flag.Parse()
	config = configuration.Read(*file)

	// MongoDB
	if err := mongodb.Connect(&config.Mongo); err != nil {
		panic(err)
	}

	// WarcraftLogs
	wcl.V1["classic"].SetTokens([]string{config.WarcraftLogs.Key})
	wcl.V1["retail"].SetTokens([]string{config.WarcraftLogs.Key})

	// Blizzard
	credentials := []oauth2.Credential{
		oauth2.Credential{
			ID:     config.Blizzard.ClientID,
			Secret: config.Blizzard.ClientSecret,
		},
	}
	blizzard.Init(credentials)
	return config
}
