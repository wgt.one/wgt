module gitlab.com/wgt.one/wgt

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/gosimple/slug v1.9.0
	gitlab.com/rakshazi/warcraftlogs-api v0.2.5
	go.mongodb.org/mongo-driver v1.4.4
)
